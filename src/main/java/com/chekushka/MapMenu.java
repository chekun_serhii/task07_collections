package com.chekushka;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MapMenu {
    private static Logger logger = LogManager.getLogger(MapMenu.class);
    Scanner input = new Scanner(System.in);
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapExecution;

    public MapMenu() {
        menuMap = new LinkedHashMap<>();
        menuMap.put("1", "1 - first menu option");
        menuMap.put("2", "2 - second menu option.");
        menuMap.put("Q", "Q - Quit");

        menuMapExecution = new LinkedHashMap<>();
        menuMapExecution.put("1", () -> logger.info("Completing the first option"));
        menuMapExecution.put("2", () -> logger.info("Completing the second option"));
    }

    private void menu() {
        logger.info("\nWelcoming message!\n");
        for (String str : menuMap.values()) {
            logger.info(str);
        }
    }

    public void start() {
        String key = null;
        do {
            menu();
            logger.info("Please select an option:");
            key = input.next();
            try {
                menuMapExecution.get(key).print();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        } while (!key.equalsIgnoreCase("e"));
    }

}

