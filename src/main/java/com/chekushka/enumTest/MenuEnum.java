package com.chekushka.enumTest;

public enum MenuEnum {
    FIRST_OPTION ("The first option"),
    SECOND_OPTION ("The second option");

    private String message;

    MenuEnum(String title) {
        this.message = title;
    }

    public String getMessage() {
        return message;
    }
}
