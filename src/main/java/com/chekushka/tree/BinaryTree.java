package com.chekushka.tree;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BinaryTree {
    private Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(BinaryTree.class);
    Node head;

    BinaryTree() {
        head = null;
    }

    public void createNode(Node temp, Node newNode) {

        if (head == null) {
            logger.info("No value exist in tree, the value just entered is set to Root");
            head = newNode;
            return;
        }
        if (temp == null)
            temp = head;

        logger.info("where you want to insert this value, l for left of (" + temp.data + ") ,r for right of (" + temp.data + ")");
        char inputValue = input.next().charAt(0);
        if (inputValue == 'l') {
            if (temp.left == null) {
                temp.left = newNode;
                logger.info("value got successfully added to left of (" + temp.data + ")");
                return;
            } else {
                logger.info("value left to (" + temp.data + ") is occupied 1by (" + temp.left.data + ")");
                createNode(temp.left, newNode);
            }
        } else if (inputValue == 'r') {
            if (temp.right == null) {
                temp.right = newNode;
                logger.info("value got successfully added to right of (" + temp.data + ")");
            } else {
                System.out.println("value right to (" + temp.data + ") is occupied by (" + temp.right.data + ")");
                createNode(temp.right, newNode);
            }

        } else {
            logger.info("incorrect input, please try again!");
        }

    }

    public Node generateTree() {
        int[] a = new int[10];
        int index = 0;
        while (index < a.length) {
            a[index] = getData();
            index++;
        }
        if (a.length == 0) {
            return null;
        }
        Node newNode = new Node();
        return generateTreeWithArray(newNode, a, 0);

    }

    public Node generateTreeWithArray(Node head, int[] a, int index) {

        if (index >= a.length)
            return null;
        logger.info("at index " + index + " value is " + a[index]);
        if (head == null)
            head = new Node();
        head.data = a[index];
        head.left = generateTreeWithArray(head.left, a, index * 2 + 1);
        head.right = generateTreeWithArray(head.right, a, index * 2 + 2);
        return head;
    }

    public Integer getData() {
        logger.info("Enter the value to insert:");
        return (Integer) input.nextInt();
    }

    public void print() {
        inorder(head);
    }

    public void inorder(Node node) {
        if (node != null) {
            inorder(node.left);
            logger.info(node.data);
            inorder(node.right);
        }
    }
}


