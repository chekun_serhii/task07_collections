package com.chekushka.tree;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class TreeApp {
    static BinaryTree treeObj = null;
    static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(BinaryTree.class);

    public static void displayMenu() {
        int choice;
        do {
            logger.info("\n Operations on a tree:");
            logger.info("\n 1. Create tree  \n 2. Insert \n 3. Search value \n " +
                    "4. print list \n Else. Exit \n Choice:");
            choice = input.nextInt();

            switch (choice) {
                case 1:
                    treeObj = createBTree();
                    logger.info("Tree created!");
                    break;
                case 2:
                    Node newNode = new Node();
                    newNode.data = getData();
                    newNode.left = null;
                    newNode.right = null;
                    treeObj.createNode(treeObj.head, newNode);
                    break;
                case 3:
                    logger.info("inorder traversal of list gives follows");
                    treeObj.print();
                    break;
                case 4:
                    Node tempHead = treeObj.generateTree();
                    logger.info("inorder traversal of list with head = (" + tempHead.data + ")gives follows");
                    treeObj.inorder(tempHead);
                    break;
                default:
                    return;
            }
        } while (true);
    }

    public static Integer getData() {
        logger.info("Enter the value to insert:");
        return (Integer) input.nextInt();
    }

    public static BinaryTree createBTree() {
        return new BinaryTree();
    }

    public static void main(String[] args) {
        displayMenu();
    }
}
